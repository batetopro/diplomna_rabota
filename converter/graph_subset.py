from rdflib import Namespace, Literal, Graph


class GraphSubset:
    def __init__(self, namespace):
        """
        :param namespace: Namespace of the RDF graph
        """
        self.namespace_uri = namespace
        self.namespace = Namespace(namespace)

    def save(self, subset, target):
        """
        Saves a list of RDF triples to a file
        :param subset: list of triples
        :param target: target file
        :return: void
        """
        from settings import Settings

        graph = Graph()
        for triple in subset:
            graph.add(triple)

        with open(target, mode="wb") as file:
            file.write(graph.serialize(format=Settings.RDF['format']))

    def exists(self, name, value, graph):
        """
        Checks if an element exists in the graph, identifying it by name ane value
        :param name: name of the element
        :param value: identifier of the element
        :param graph:
        :return: bool
        """
        uri = self.namespace['{0}/{1}'.format(name, value)]
        return self.cardinality(uri, graph) > 0

    def parse_uri(self, uri):
        """
        Parse URI to a name and identifier of a node
        :param uri:
        :return: dict
        """
        uri = uri.replace(self.namespace_uri, '')
        parts = uri.split('/', 1)
        result = dict(name=parts[0], value=None)

        if len(parts) > 1:
            result['value'] = parts[1]

        return result

    @staticmethod
    def cardinality(uri, graph):
        """
        Returns the count of the child elements of a element identified by URI
        :param uri: URI of the element
        :param graph: the graph, which is searched
        :return: int
        """
        result = 0

        for _ in graph.triples((uri, None, None)):
            result += 1

        return result

    def _fetch_list(self, uri, graph, restricted):
        """
        Internal fetch
        :param uri: URI of the element, from which the lookup is started
        :param graph: the graph, which is searched
        :param restricted: check if the lookup should be restricted, so that the element is the only one child element
        :return: list
        """
        queue = [(uri, None, None)]
        result = []

        while len(queue) > 0:
            item = queue.pop()

            for s, p, o in graph.triples((item[0], None, None)):
                if restricted and self.cardinality(item[0], graph) == 1:
                    continue

                result.append([s, p, o])

                if not isinstance(o, Literal):
                    queue.append([o, None, None])

        return result

    def _fetch_dict(self, uri, graph):
        """
        Internal fetch
        :param uri: URI of the element, from which the lookup is started
        :param graph: graph structure
        :return: dict
        """
        parts = self.parse_uri(uri)

        result = {
            "tag": parts['name'],
            "value": None,
            "children": [],
            "attributes": {}
        }

        for s, p, o in graph.triples((uri, None, None)):
            if not isinstance(o, Literal):
                result["children"].append(self._fetch_dict(o, graph))
            else:
                parts = self.parse_uri(p)
                result["children"].append(
                    {
                        "tag": parts['name'],
                        "value": o,
                        "children": [],
                        "attributes": {}
                    }
                )

        return result

    def get_dict(self, name, value, graph):
        """
        Make a sub graph of a given graph and express the result in a recursive dictionary structure
        :param name: name of the element
        :param value: identifier of the element
        :param graph: graph structure
        :return: dict
        """
        uri = self.namespace['{0}/{1}'.format(name, value)]
        return self._fetch_dict(uri, graph)

    def get_list(self, name, value, graph, restricted=False):
        """
        Make a sub graph of a given graph and express the result as a list of RDF triples
        :param name: name of the element
        :param value: identifier of the element
        :param graph: graph structure
        :param restricted: should the elements do not have any other incoming edges in the graph
        :return: list
        """
        uri = self.namespace['{0}/{1}'.format(name, value)]
        return self._fetch_list(uri, graph, restricted)
