import hashlib
from rdflib import Literal, URIRef, Namespace


class RdfBuilder:
    def __init__(self, namespace, identifiers):
        """
        :param namespace: Namespace of the RDF graph
        :param identifiers: File containing the definitions of the identifiers
        """
        self.namespace_uri = namespace
        self.namespace = Namespace(namespace)
        self.identifiers = identifiers

    @staticmethod
    def get_child_value(name, children):
        """
        Get a child value in a list of items
        :param name: name of the tag
        :param children: list with items
        """
        for item in children:
            if item['tag'] == name:
                return item['value']

    def parse_uri(self, uri):
        """
        Parse URI to a name and identifier of a node
        :param uri:
        :return: dict
        """
        uri = uri.replace(self.namespace_uri, '')
        parts = uri.split('/', 1)
        result = dict(name=parts[0], value=None)

        if len(parts) > 1:
            result['value'] = parts[1]

        return result

    @staticmethod
    def get_children_signature(item):
        """
        Get a cryptographic signature of the children of a item
        :param item: Dict describing an element
        :return: string
        """
        tags = sorted([c['tag'] for c in item['children']])

        children = {}
        for c in item['children']:
            children[c['tag']] = c

        values = [children[t]['value'] for t in tags]

        return hashlib.sha1('@'.join(values).encode()).hexdigest()

    def get_node(self, item, parent):
        """
        Make a URIRef from a graph node, describing in it by its dict and parent dict structures.
        :param item: Dict describing the element
        :param parent: Dict describing the parent element
        :return: URIRef
        """
        if item['tag'] in self.identifiers:
            identifier = self.identifiers[item['tag']]

            if identifier['children']:
                return URIRef(self.namespace["{0}/{1}".format(item['tag'], self.get_children_signature(item))])

            if identifier['id'] is None:
                return URIRef(self.namespace["{0}/{1}".format(item['tag'], self.identifiers.get_counter(item['tag']))])

            child_value = self.get_child_value(identifier['id'], item['children'])

            if identifier['parent'] is None:
                return URIRef(self.namespace["{0}/{1}".format(item['tag'], child_value)])

            parent_value = self.get_child_value(identifier['parent'], parent['children'])

            return URIRef(self.namespace["{0}/{1}@{2}".format(item['tag'], child_value, parent_value)])

        return URIRef(self.namespace["{0}/{1}".format(item['tag'], self.identifiers.get_counter(item['tag']))])

    def __parse(self, item, parent, graph):
        """
        Parse elements a level in the graph and add the URIRefs in a graph
        :param item: dict structure
        :param parent: parent structure
        :param graph: the RDF graph
        :return: bool, tag, URIRef
        """

        if len(item['children']) == 0:
            return True, item['tag'], item['value']

        node = self.get_node(item, parent)

        for child in item['children']:
            is_literal, name, value = self.__parse(child, item, graph)

            if is_literal:
                graph.add((node, self.namespace[name], Literal(value)))
            else:
                graph.add((node, self.namespace["{0}{1}".format(item['tag'], name)], value))

        return False, item['tag'], node

    def parse(self, data, parent, graph):
        """
        Parse dict structure and load it in a RDF graph
        :param data: dict structure
        :param parent: parent structure
        :param graph: the RDF graph
        :return: bool, tag, URIRef
        """
        self.__parse(data, parent, graph)
