from lxml import etree
import xml.etree.ElementTree as ET


class XmlParser:
    def __init__(self, schema_path):
        """
        :param schema_path: XSD definition file
        """
        xmlschema_doc = etree.parse(schema_path)
        self.xmlschema = etree.XMLSchema(xmlschema_doc)

    def parse(self, element):
        """
        :param element: Parse XML element in a dict structure
        """
        tag = element.tag

        if '}' in tag:
            tag = tag.split('}')[1]

        text = element.text
        if text is not None:
            text = text.strip()

        result = {"tag": tag, "value": text, "children": [], "attributes": {}}

        for child in element:
            result["children"].append(self.parse(child))

        for k in element.attrib:
            result["attributes"][k] = element.attrib[k]

        return result

    def build_node(self, data):
        """
        :param data: Build a XML Element as a dict structure
        """

        node = etree.Element(data['tag'])

        if data['value'] is not None:
            node.text = data['value']

        for a in data['attributes']:
            node.attrib[a] = data['attributes'][a]

        for c in data['children']:
            node.append(self.build_node(c))

        return node

    def build(self, data):
        """
        :param data: Build a XML string from a dict structure
        """
        node = self.build_node(data)
        return etree.tostring(node, pretty_print=True, encoding='unicode')

    def validate(self, content):
        """
        Validate a XML string
        :param content: the XML as a string
        :return: bool
        """
        xml_doc = etree.fromstring(content)
        return self.xmlschema.validate(xml_doc)

    def validate_file(self, path):
        """
        Validate the content in a XML file
        :param path: path to the XML
        :return: bool
        """
        xml_doc = etree.parse(path)
        return self.xmlschema.validate(xml_doc)

    def convert(self, value):
        """
        :param value: Convert a XML string ot a dict structure
        """
        root = ET.fromstring(value)
        return self.parse(root)

    def convert_file(self, path):
        """
        Convert XML file to a dict structure
        :param path: XML file path
        """
        tree = ET.parse(path)
        return self.parse(tree.getroot())

    @staticmethod
    def get_value(data, path):
        """
        Get the value of an element from a dict
        :param data: The dict
        :param path: Path in the dict
        """
        parts = path.split('/')

        while len(parts) > 0:
            tag = parts.pop(0)
            for c in data['children']:
                if c['tag'] == tag:
                    data = c
                    if len(parts) == 0:
                        return c['value']

        return None
