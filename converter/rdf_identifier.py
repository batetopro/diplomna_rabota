import csv


class RdfIdentifier:
    def __init__(self, path):
        """
        :param path: Filename to the RDF identifiers
        """
        self.data = {}

        with open(path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)

            for row in csv_reader:
                for k in row:
                    if row[k] is None:
                        continue

                    row[k] = row[k].strip()
                    if len(row[k]) == 0:
                        row[k] = None

                item = dict(
                    id=row['id'],
                    parent=row['parent'],
                    children=False,
                    counter=0
                )

                if row['children'] is not None:
                    item['children'] = True

                self.data[row['object']] = item

    def get_counter(self, tag):
        """
        Get the current element counter of a given tag
        :param tag: tag
        :return: int
        """
        if tag not in self.data:
            self.data['counter'] = 0
        value = self.data[tag]['counter']
        self.data[tag]['counter'] += 1
        return value

    def __contains__(self, tag):
        """
        Check if given tag is contained in the collection
        :param tag: string
        :return: bool
        """
        return tag in self.data

    def __getitem__(self, tag):
        """
        Get the data for a given tag
        :param tag: string
        :return: dict
        """
        return self.data[tag]
