import os


class Settings:
    RDF = {
        'namespace': 'http://amblist.com/',
        'format': 'xml',
    }

    FLASK_APP = {
        'MAX_CONTENT_LENGTH': 1600 * 1024 * 1024
    }

    REST = {
        'port': 8090,
        'host': '0.0.0.0'
    }

    PATH = {
        'storage': os.path.join('storage'),
        'root': os.path.join('data'),
        'convert': os.path.join('data', 'convert'),
        'dist': os.path.join('data', 'dist'),
        'amblist': os.path.join('data', 'amblist'),
        'owl': os.path.join('data', 'amblist.owl'),
        'definition': os.path.join('data', 'definition.xsd'),
        'identifiers': os.path.join('data', 'identifiers.csv'),
    }

