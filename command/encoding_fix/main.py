import os
from lxml import etree
from io import BytesIO


class EncodingFixRunner:
    def execute(self):
        from settings import Settings

        xmlschema = etree.XMLSchema(file=Settings.PATH['definition'])

        for root, folder, files in os.walk(Settings.PATH['amblist']):
            for file in files:
                path = os.path.join(root, file)

                with open(path, 'rb') as f:
                    text = f.read()

                parser = etree.XMLParser(remove_blank_text=True)
                tree = etree.parse(BytesIO(text), parser)

                try:
                    xmlschema.validate(tree)
                except:
                    print('[NOT VALID] {0}'.format(path))
                    continue

                tree.write(path, encoding='utf-8', pretty_print=True)

                print('[FIXED] {0}'.format(path))

