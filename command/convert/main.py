import os
from converter import XmlParser, RdfBuilder, RdfIdentifier
from rdflib import Graph


class ConvertRunner:
    def __init__(self):
        from settings import Settings
        identifiers = RdfIdentifier(Settings.PATH['identifiers'])
        self.builder = RdfBuilder(Settings.RDF['namespace'], identifiers)
        self.parser = XmlParser(Settings.PATH['definition'])

    def convert(self, source):
        from settings import Settings

        if not os.path.exists(source):
            raise FileNotFoundError('File does not exists: {0}'.format(source))

        try:
            self.parser.validate_file(source)
        except Exception as e:
            raise ValueError('XML file {0} is not valid.'.format(source), e)

        data = self.parser.convert_file(source)

        uin = self.parser.get_value(data, 'Doctor/UIN')
        if uin is None:
            raise ValueError('UIN is not set for the doctor')

        target = os.path.join(Settings.PATH['dist'], uin.replace('/', '').replace('\\', ''))

        graph = Graph()
        if os.path.exists(target):
            graph.parse(target, format=Settings.RDF['format'])

        self.builder.parse(data, None, graph)

        with open(target, mode="wb") as file:
            file.write(graph.serialize(format=Settings.RDF['format']))

        os.unlink(source)

    def execute(self):
        from settings import Settings

        for root, folders, files in os.walk(Settings.PATH['convert'], topdown=True):
            for file in files:
                filename = os.path.join(root, file)

                try:
                    self.convert(filename)
                    print('[OK] {0}'.format(filename))
                except Exception as e:
                    print('[FAIL] {0} = {1}'.format(filename, e))
