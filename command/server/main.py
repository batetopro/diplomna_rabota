from server import build_server


class ServerRunner:
    def execute(self):
        from settings import Settings

        app = build_server()

        app.run(
            host=Settings.REST['host'],
            port=Settings.REST['port']
        )
