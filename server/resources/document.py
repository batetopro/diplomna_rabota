from flask import Response
from flask_restful import Resource, reqparse


document_parser = reqparse.RequestParser()


document_parser.add_argument(
    'number',
    dest='number',
    location='args',
    required=True,
    help='number'
)

document_parser.add_argument(
    'doctor',
    dest='doctor',
    location='args',
    required=True,
    help='doctor'
)


class DocumentDoesNotExists(Exception):
    pass


class ApiDocument(Resource):
    def __init__(self, **kwargs):
        self.xml_parser = kwargs['xml_parser']
        self.rdf_builder = kwargs['rdf_builder']
        self.graph = kwargs['graph']
        self.graph_subset = kwargs['graph_subset']

    def get(self):
        args = document_parser.parse_args()

        if not self.graph_subset.exists('AmbList', '{0}@{1}'.format(args['number'], args['doctor']), self.graph):
            raise DocumentDoesNotExists()

        data = self.graph_subset.get_dict(
            'AmbList',
            '{0}@{1}'.format(args['number'], args['doctor']),
            self.graph
        )

        content = self.xml_parser.build(data)

        return Response(content, 200)
