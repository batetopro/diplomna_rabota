import os
from flask import Response
from flask_restful import Resource, reqparse

send_parser = reqparse.RequestParser()

send_parser.add_argument(
    'document',
    dest='document',
    location='form',
    required=True,
    help='document'
)

send_parser.add_argument(
    'doctor',
    dest='doctor',
    location='form',
    required=True,
    help='doctor'
)


class DocumentAlreadyExists(Exception):
    pass


class DoctorDoesNotExists(Exception):
    pass


class ApiSend(Resource):
    def __init__(self, **kwargs):
        self.graph = kwargs['graph']
        self.xml_parser = kwargs['xml_parser']
        self.rdf_builder = kwargs['rdf_builder']
        self.graph_subset = kwargs['graph_subset']

    def post(self):
        from settings import Settings

        args = send_parser.parse_args()

        self.xml_parser.validate(args['document'])
        data = self.xml_parser.convert(args['document'])

        number = self.rdf_builder.get_child_value('NoAl', data['children'])
        if self.graph_subset.exists('AmbList', '{0}@{1}'.format(number, args['doctor']), self.graph):
            raise DocumentAlreadyExists()

        if not self.graph_subset.exists('Doctor', args['doctor'], self.graph):
            raise DoctorDoesNotExists()

        target = os.path.join(Settings.PATH['dist'], args['doctor'])

        doctor = {
            "tag": 'Dcotor',
            "value": '',
            "children": [
                {
                    "tag": 'UIN',
                    "value": args['doctor'],
                    "children": [],
                    "attributes": {}
                }
            ],
            "attributes": {}
        }

        self.rdf_builder.parse(data, doctor, self.graph)

        target = target.replace('/', '').replace('\\', '')

        with open(target, mode="wb") as file:
            file.write(self.graph.serialize(format=Settings.RDF['format']))

        return Response(dict(), 200)
