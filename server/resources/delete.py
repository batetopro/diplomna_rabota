import os
from flask_restful import Resource, reqparse


delete_parser = reqparse.RequestParser()

delete_parser.add_argument(
    'number',
    dest='number',
    location='args',
    required=True,
    help='document'
)

delete_parser.add_argument(
    'doctor',
    dest='doctor',
    location='args',
    required=True,
    help='doctor'
)


class DocumentDoesNotExists(Exception):
    pass


class DoctorDoesNotExits(Exception):
    pass


class ApiDelete(Resource):
    def __init__(self, **kwargs):
        self.graph = kwargs['graph']
        self.graph_subset = kwargs['graph_subset']

    def get(self):
        from settings import Settings

        args = delete_parser.parse_args()

        if not self.graph_subset.exists('AmbList', '{0}@{1}'.format(args['number'], args['doctor']), self.graph):
            raise DocumentDoesNotExists()

        if not self.graph_subset.exists('Doctor', args['doctor'], self.graph):
            raise DoctorDoesNotExits()

        triples = self.graph_subset.get_list(
            'AmbList',
            '{0}@{1}'.format(args['number'], args['doctor']),
            self.graph,
            True
        )

        for triple in triples:
            self.graph.remove(triple)

        doctor = self.graph_subset.get_list(
            'Doctor',
            '{0}'.format(args['doctor']),
            self.graph,
            True
        )

        target = os.path.join(Settings.PATH['dist'], args['doctor'])
        self.graph_subset.save(doctor, target)

        return dict(error=0, message='OK')
