from flask import Response
from flask_restful import Resource, reqparse


export_parser = reqparse.RequestParser()


export_parser.add_argument(
    'format',
    dest='format',
    default='xml',
    required=False,
    help='format',
    choices=['xml', 'n3', 'turtle', 'nt', 'pretty-xml', 'trix', 'trig', 'nquads']
)


class ApiExport(Resource):
    def __init__(self, **kwargs):
        self.graph = kwargs['graph']

    def get(self):
        args = export_parser.parse_args()
        return Response(self.graph.serialize(format=args['format']), 200)
