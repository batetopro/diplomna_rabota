import os
import tempfile
from flask import Response
from werkzeug.datastructures import FileStorage
from flask_restful import Resource, reqparse


upload_parser = reqparse.RequestParser()


upload_parser.add_argument(
    'file',
    dest='file',
    location='files',
    type=FileStorage,
    required=True,
    help='file',
)


class ApiUploadFile(Resource):
    def __init__(self, **kwargs):
        self.graph = kwargs['graph']
        self.xml_parser = kwargs['xml_parser']
        self.rdf_builder = kwargs['rdf_builder']
        self.graph_subset = kwargs['graph_subset']

    def post(self):
        from settings import Settings

        args = upload_parser.parse_args()

        content = args['file'].stream.read()
        self.xml_parser.validate(content)

        with tempfile.TemporaryFile() as fp:
            fp.write(content)

        parsed = self.xml_parser.convert(content)
        uin = self.xml_parser.get_value(parsed, 'Doctor/UIN')

        self.rdf_builder.parse(parsed, None, self.graph)

        subset = self.graph_subset.get_list('Doctor', uin, self.graph)

        target = os.path.join(Settings.PATH['dist'], uin.replace('/', '').replace('\\', ''))
        self.graph_subset.save(subset, target)

        return Response(dict(), 200)
