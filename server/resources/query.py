from flask import Response
from flask_restful import Resource, reqparse


query_parser = reqparse.RequestParser()


query_parser.add_argument(
    'query',
    dest='query',
    location='form',
    required=True,
    help='query'
)

query_parser.add_argument(
    'format',
    dest='format',
    location='form',
    default='xml',
    required=False,
    help='format',
    choices=['xml', 'json']
)


class ApiQuery(Resource):
    def __init__(self, **kwargs):
        self.graph = kwargs['graph']

    def post(self):
        args = query_parser.parse_args()
        result = self.graph.query(args['query'])
        return Response(result.serialize(format=args['format']), 200)
