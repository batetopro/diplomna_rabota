import os
from flask import Flask
from flask_restful import Api

from converter import RdfBuilder, XmlParser, RdfIdentifier, GraphSubset
from rdflib import Graph

from .resources import *


errors = {
    'ParseException': {
        'message': "Invalid SPARQL query was provided.",
        'status': 500,
    },
    'DocumentAlreadyExists': {
        'message': "Document already exists.",
        'status': 500,
    },
    'DoctorDoesNotExits': {
        'message': "Doctor does not exist.",
        'status': 500,
    },
    'DocumentDoesNotExists': {
        'message': "Document does not exist.",
        'status': 500,
    },
    'DoctorDoesNotExists': {
        'message': "Doctor does not exist.",
        'status': 500,
    },

}


def build_server():
    from settings import Settings

    graph = Graph()
    xml_parser = XmlParser(Settings.PATH['definition'])
    identifiers = RdfIdentifier(Settings.PATH['identifiers'])
    rdf_builder = RdfBuilder(Settings.RDF['namespace'], identifiers)
    graph_subset = GraphSubset(Settings.RDF['namespace'])

    for root, folders, files in os.walk(Settings.PATH['dist']):
        for file in files:
            print('[LOAD] {0}'.format(file))
            graph.parse(os.path.join(root, file))

    kwargs = {
        'graph': graph,
        'xml_parser': xml_parser,
        'rdf_builder': rdf_builder,
        'graph_subset': graph_subset,
    }

    app = Flask(__name__)

    for k in Settings.FLASK_APP:
        app.config[k] = Settings.FLASK_APP[k]

    api = Api(app, errors=errors)
    api.add_resource(ApiQuery,       '/query',     resource_class_kwargs=kwargs)
    api.add_resource(ApiDocument,    '/document',  resource_class_kwargs=kwargs)
    api.add_resource(ApiUploadFile,  '/upload',    resource_class_kwargs=kwargs)
    api.add_resource(ApiExport,      '/export',    resource_class_kwargs=kwargs)
    api.add_resource(ApiSend,        '/send',      resource_class_kwargs=kwargs)
    api.add_resource(ApiDelete,      '/delete',    resource_class_kwargs=kwargs)

    @app.route('/', methods=['GET'])
    def home():
        return app.send_static_file('index.html')

    @app.route('/<path:path>', methods=['GET'])
    def static_file(path):
        return app.send_static_file(path)

    return app
